﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace M2Link.Domain
{
    public class User : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Pseudo { get; set; }
        public string Description { get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
		public virtual Document Image { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual ICollection<User> Subscriptions { get; set; }

        public User()
        {
        }

        public void Update(string firstname, string lastname, string email, string pseudo, string description, Document document = null)
        {
            FirstName = firstname;
            LastName = lastname;
            Email = email;
            Pseudo = pseudo;
            Description = description;
            if (document != null)
            {
                Image = document;
            }
        }

        public void Follow(User subscription)
        {
            if (!Subscriptions.Any(o => o.Id == subscription.Id))
            {
                Subscriptions.Add(subscription);
            }
        }

        public void Unfollow(User subscription)
        {
            Subscriptions.Remove(subscription);
        }
    }
}