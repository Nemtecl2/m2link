﻿using System;
using System.ComponentModel.DataAnnotations;

namespace M2Link.Domain
{
    public class Entity
    {
        [KeyAttribute]
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }

        public Entity()
        {
            Id = Guid.NewGuid();
            CreatedAt = DateTime.Now;
        }
    }
}