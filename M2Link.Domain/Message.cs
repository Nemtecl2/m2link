﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace M2Link.Domain
{
    public class Message : Entity
    {
        public string Content { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<Like> Likes { get; set; }

        public Message()
        {
        }
    }
}