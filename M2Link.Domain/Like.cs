﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace M2Link.Domain
{
    public class Like : Entity
    {
        public virtual User User { get; set; }
        public virtual Message Message { get; set; }

        public Like()
        {
        }
    }
}