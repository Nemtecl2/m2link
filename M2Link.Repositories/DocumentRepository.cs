﻿using M2Link.Domain;
using M2Link.Persistence;
using System;
using System.Linq;

namespace M2Link.Repositories
{
    public class DocumentRepository : Repository<Document>
    {
        public DocumentRepository(M2LinkContext context) : base(context)
        {
        }
    }
}