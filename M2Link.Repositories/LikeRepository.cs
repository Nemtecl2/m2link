﻿using M2Link.Domain;
using M2Link.Persistence;
using System;
using System.Linq;

namespace M2Link.Repositories
{
    public class LikeRepository : Repository<Like>
    {
        public LikeRepository(M2LinkContext context) : base(context)
        {
        }

        public bool Exists(Guid userId, Guid messageId)
        {
            return GetAll().Any(o => o.User.Id == userId && o.Message.Id == messageId);
        }

        public Like GetByUserAndMessageId(Guid userId, Guid messageId)
        {
            return GetAll().FirstOrDefault(o => o.User.Id == userId && o.Message.Id == messageId);
        }
    }
}