﻿using M2Link.Domain;
using M2Link.Persistence;

namespace M2Link.Repositories
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(M2LinkContext context) : base(context)
        {
        }
    }
}