﻿using M2Link.Domain;
using M2Link.Persistence;

namespace M2Link.Repositories
{
    public class MessageRepository : Repository<Message>
    {
        public MessageRepository(M2LinkContext context) : base(context)
        {
        }
    }
}