﻿using M2Link.Domain;
using M2Link.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2Link.Repositories
{
    public abstract class Repository<TEntity> 
       where TEntity : Entity
    {
        /// <summary>
        /// Contexte sous jacent à ce dépot
        /// </summary>
        public M2LinkContext Context { get; set; }

        protected Repository(M2LinkContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Méthode permettant de retourner les entités de base.
        /// </summary>
        /// <returns></returns>
        public virtual List<TEntity> GetAll()
        {
            return Context.Set<TEntity>().ToList();
        }

        public virtual void Add(TEntity entity)
        {
            Context.Set<TEntity>().Add(entity);
        }

        public virtual TEntity GetById(Guid id)
        {
            return GetAll().FirstOrDefault(o => o.Id == id);
        }

        public virtual void Delete(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }
    }
}