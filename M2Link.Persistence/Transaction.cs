﻿using System;

namespace M2Link.Persistence
{
    public sealed class Transaction : IDisposable
    {
        public readonly M2LinkContext Context;

        public Transaction(M2LinkContext context)
        {
            Context = context;
        }

        public void Commit()
        {
            Context.SaveChanges();
        }

        public void Dispose()
        {
            Commit();
        }
    }
}