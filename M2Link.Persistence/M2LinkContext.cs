﻿
using M2Link.Domain;
using System.Data.Entity;

namespace M2Link.Persistence
{
    public class M2LinkContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Like> Likes { get; set; }
		public DbSet<Document> Documents { get; set; }

		public M2LinkContext() : base("M2LinkContext")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>()
            .HasMany(p => p.Subscriptions)
            .WithMany()
            .Map(m =>
            {
                m.MapLeftKey("UserID");
                m.MapRightKey("SubscriptionID");
                m.ToTable("Subscriptions");
            });
        }
    }
}