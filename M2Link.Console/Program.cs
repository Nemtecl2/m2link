﻿using M2Link.Console.ExternalWebService;
using System;

namespace M2Link.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                WSM2LinkClient serviceClient = new WSM2LinkClient();

                System.Console.Write("Username : ");
                string username = System.Console.ReadLine();
                System.Console.Write("Password : ");
                string lastname = System.Console.ReadLine();

                var user = serviceClient.Validate(username, lastname);
                if (user != null)
                {
                    System.Console.WriteLine(user.Id);
                    System.Console.WriteLine(user.Email);
                    System.Console.WriteLine(user.FirstName + " " + user.LastName);
                    System.Console.WriteLine(user.Pseudo);

                }
                else
                {
                    System.Console.WriteLine("Mauvais couple username / mot de passe");
                }
                System.Console.WriteLine(Environment.NewLine);

            }
        }
    }
}
