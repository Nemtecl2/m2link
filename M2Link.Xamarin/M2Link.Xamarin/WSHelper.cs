﻿using M2Link.Xamarin.WebServiceClients;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;

namespace M2Link.Xamarin
{
    public class WSHelper
    {
        public static WSM2LinkClient WSM2LinkClient = new WSM2LinkClient(new BasicHttpBinding(), new EndpointAddress(AppConstants.WSServer + "/WebServices/WSM2Link.svc"));
    }
}
