﻿using M2Link.Xamarin.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace M2Link.Xamarin.ViewModels
{
    public class MessagesViewModel : BaseViewModel
    {
        private readonly INavigation Navigation;
        private readonly MessagesPage Parent;

        public MessagesViewModel(INavigation _navigation, MessagesPage parent)
        {
            Navigation = _navigation;
            PageTitle = "Messages";
            Parent = parent;
            var timeline = WSHelper.WSM2LinkClient.Timeline(AppConstants.User.Id).Messages.ToList();
            var list = timeline.Select(o => new MessageType
            {
                Parent = Parent,
                Id = o.Id,
                IsLiked = o.IsLiked,
                Likes = o.Likes == null ? 0 : o.Likes.Length,
                UserFullname = o.UserFirstName + " " + o.UserLastName + " (@" + o.UserPseudo + ")",
                UserId = o.UserId,
                Content = o.Content,
                CreatedAt = o.CreatedAt,
                ImageUrl = Convert.FromBase64String(o.ImageUrl.Replace("data:image;base64,", "")),
            }).ToList();

            Messages = new ObservableCollection<MessageType>();
            foreach (var elem in list)
            {
                Messages.Add(elem);
            }
        }

        #region Properties

        private ObservableCollection<MessageType> _messages;
        public ObservableCollection<MessageType> Messages
        {
            get { return _messages; }
            set
            {
                SetPropertyValue(ref _messages, value);
            }
        }


        #endregion

        #region Commands

        

        #endregion
    }

    public class MessageType
    {
        public MessagesPage Parent { get; set; }
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Content { get; set; }
        public Guid UserId { get; set; }
        public string UserFullname { get; set; }
        public byte[] ImageUrl { get; set; }
        public int Likes { get; set; }
        public bool IsLiked { get; set; }
        public string NbLikeLabel { get
            {
                return Likes + " J'aime" + (Likes > 1 ? "s" : "");
            }
        }

        private ICommand _buttonCommand;
        public ICommand ButtonCommand
        {
            get { return _buttonCommand = _buttonCommand ?? new Command(ButtonAction); }
        }

        void ButtonAction()
        {
            if (IsLiked)
            {
                WSHelper.WSM2LinkClient.Dislike(AppConstants.User.Id, Id);
                Parent.DisplayAlert("Information", "Vous n'aimez plus ce message", "Ok");
                Parent.Refresh();
            }
            else
            {
                WSHelper.WSM2LinkClient.Like(AppConstants.User.Id, Id);
                Parent.DisplayAlert("Information", "Vous aimez ce message", "Ok");
                Parent.Refresh();
            }

        }

        public string LikeLabel
        {
            get
            {
                return IsLiked ? "Je n'aime plus" : "J'aime";
            }
        }

        public MessageType()
        {
        }
    }
}