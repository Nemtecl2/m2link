﻿using M2Link.Xamarin.Views;
using M2Link.Xamarin.WebServiceClients;
using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace M2Link.Xamarin.ViewModels
{
    public class ProfileViewModel : BaseViewModel
    {
        private readonly INavigation Navigation;
        private readonly ProfilePage Parent;

        public ProfileViewModel(INavigation _navigation, ProfilePage parent, SimpleUserData data)
        {
            Navigation = _navigation;
            PageTitle = "Profil";
            Parent = parent;
            Firstname = data.FirstName;
            Lastname = data.LastName;
            Email = data.Email;
            Image = Convert.FromBase64String(data.ImageUrl.Replace("data:image;base64,", ""));
            Pseudo = data.Pseudo;
            Description = data.Description;
            CreatedAt = data.CreatedAt;
        }

        #region Properties

        private string _firstname;
        public string Firstname
        {
            get { return _firstname; }
            set
            {
                SetPropertyValue(ref _firstname, value);
            }
        }

        private string _lastname;
        public string Lastname
        {
            get { return _lastname; }
            set
            {
                SetPropertyValue(ref _lastname, value);
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                SetPropertyValue(ref _email, value);
            }
        }

        private string _pseudo;
        public string Pseudo
        {
            get { return _pseudo; }
            set
            {
                SetPropertyValue(ref _pseudo, value);
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                SetPropertyValue(ref _description, value);
            }
        }

        private byte[] _image;
        public byte[] Image
        {
            get { return _image; }
            set
            {
                SetPropertyValue(ref _image, value);
            }
        }

        private DateTime _createdAt;
        public DateTime CreatedAt
        {
            get { return _createdAt; }
            set
            {
                SetPropertyValue(ref _createdAt, value);
            }
        }

        #endregion


        #region Commands

        private ICommand _disconnectCommand;
        public ICommand DisconnectCommand
        {
            get { return _disconnectCommand = _disconnectCommand ?? new Command(DisconnectAction); }
        }


        #endregion


        #region Methods


        void DisconnectAction()
        {
            AppConstants.User = null;
            Navigation.PushAsync(new LoginPage());
        }

        #endregion
    }
}
