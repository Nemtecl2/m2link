﻿using M2Link.Xamarin.Views;
using System;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace M2Link.Xamarin.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        private readonly INavigation Navigation;
        private readonly RegisterPage Parent;

        public RegisterViewModel(INavigation _navigation, RegisterPage parent)
        {
            Navigation = _navigation;
            PageTitle = "S'enregister";
            Parent = parent;
        }

        #region Properties

        private string _firstname;
        public string Firstname
        {
            get { return _firstname; }
            set
            {
                if (SetPropertyValue(ref _firstname, value))
                {
                    ((Command)RegisterCommand).ChangeCanExecute();
                }
            }
        }

        private string _lastname;
        public string Lastname
        {
            get { return _lastname; }
            set
            {
                if (SetPropertyValue(ref _lastname, value))
                {
                    ((Command)RegisterCommand).ChangeCanExecute();
                }
            }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set
            {
                if (SetPropertyValue(ref _email, value))
                {
                    ((Command)RegisterCommand).ChangeCanExecute();
                }
            }
        }

        private string _pseudo;
        public string Pseudo
        {
            get { return _pseudo; }
            set
            {
                if (SetPropertyValue(ref _pseudo, value))
                {
                    ((Command)RegisterCommand).ChangeCanExecute();
                }
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                if (SetPropertyValue(ref _description, value))
                {
                    ((Command)RegisterCommand).ChangeCanExecute();
                }
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                if (SetPropertyValue(ref _password, value))
                {
                    ((Command)RegisterCommand).ChangeCanExecute();
                }
            }
        }

        private string _confirmPassword;
        public string ConfirmPassword
        {
            get { return _confirmPassword; }
            set
            {
                if (SetPropertyValue(ref _confirmPassword, value))
                {
                    ((Command)RegisterCommand).ChangeCanExecute();
                }
            }
        }
        #endregion


        #region Commands

        private ICommand _registerCommand;
        public ICommand RegisterCommand
        {
            get { return _registerCommand = _registerCommand ?? new Command(RegisterAction, CanRegisterAction); }
        }

        private ICommand _loginCommand;
        public ICommand LoginCommand
        {
            get { return _loginCommand = _loginCommand ?? new Command(LoginAction); }
        }

        #endregion


        #region Methods

        bool CanRegisterAction()
        {
            if (string.IsNullOrWhiteSpace(Firstname) || string.IsNullOrWhiteSpace(Lastname)
                || string.IsNullOrWhiteSpace(Email) || string.IsNullOrWhiteSpace(Pseudo)
                || string.IsNullOrWhiteSpace(Password) || string.IsNullOrWhiteSpace(ConfirmPassword))
                return false;

            return true;
        }

        void RegisterAction()
        {
            Regex rgx = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,}$");
            if (!rgx.IsMatch(Password) || !rgx.IsMatch(ConfirmPassword))
            {
                Parent.DisplayAlert("Erreur", "Les mots de passes doivent être de taille 8, contenir au moins " +
                    "un chiffre, une majuscule, une minuscule et un caractère spécial", "Ok");
                return;
            }

            if (Password != ConfirmPassword)
            {
                Parent.DisplayAlert("Erreur", "Les mots de passes sont différents", "Ok");
                return;
            }

            rgx = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            if (!rgx.IsMatch(Email))
            {
                Parent.DisplayAlert("Erreur", "Veuillez saisir une adresse mail valide", "Ok");
                return;
            }

            string result = WSHelper.WSM2LinkClient.Register(Firstname, Lastname, Email, Pseudo, Password, Description);
            if (result == null)
            {
                Parent.DisplayAlert("Information", "Enregistrement effectué", "Ok");
                Navigation.PushAsync(new LoginPage());
                return;
            }
            else
            {
                Parent.DisplayAlert("Erreur", result, "Ok");
                return;
            }
        }


        void LoginAction()
        {
            Navigation.PushAsync(new LoginPage());
        }

        #endregion
    }
}
