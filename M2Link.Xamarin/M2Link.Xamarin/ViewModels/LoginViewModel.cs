﻿using M2Link.Xamarin.Views;
using M2Link.Xamarin.WebServiceClients;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace M2Link.Xamarin.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private readonly INavigation Navigation;
        private readonly LoginPage Parent;

        public LoginViewModel(INavigation _navigation, LoginPage parent)
        {
            Navigation = _navigation;
            PageTitle = "Se connecter";
            Parent = parent;
        }

        #region Properties

        private string _pseudo;
        public string Pseudo
        {
            get { return _pseudo; }
            set
            {
                if (SetPropertyValue(ref _pseudo, value))
                {
                    ((Command)LoginCommand).ChangeCanExecute();
                }
            }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set
            {
                if (SetPropertyValue(ref _password, value))
                {
                    ((Command)LoginCommand).ChangeCanExecute();
                }
            }
        }
        #endregion


        #region Commands

        private ICommand _loginCommand;
        public ICommand LoginCommand
        {
            get { return _loginCommand = _loginCommand ?? new Command(LoginAction, CanLoginAction); }
        }

        private ICommand _registerCOmmand;
        public ICommand RegisterCommand
        {
            get { return _registerCOmmand = _registerCOmmand ?? new Command(RegisterAction); }
        }

        #endregion


        #region Methods

        bool CanLoginAction()
        {
            if (string.IsNullOrWhiteSpace(this.Pseudo) || string.IsNullOrWhiteSpace(this.Password))
                return false;

            return true;
        }

        void LoginAction()
        {
            IsBusy = true;
            SimpleUserData user = WSHelper.WSM2LinkClient.Validate(Pseudo, Password);
            if (user != null)
            {
                AppConstants.User = user;
                IsBusy = false;
                Navigation.PushAsync(new HomePage());
            } else
            {
                Parent.DisplayAlert("Erreur", "Mauvais couple pseudo / mot de passe", "Ok");
                IsBusy = false;
            }
        }


        void RegisterAction()
        {
            Navigation.PushAsync(new RegisterPage());
        }

        #endregion
    }
}
