﻿using M2Link.Xamarin.Views;
using M2Link.Xamarin.WebServiceClients;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace M2Link.Xamarin.ViewModels
{
    public class CreateMessageViewModel : BaseViewModel
    {
        private readonly INavigation Navigation;
        private readonly CreateMessagePage Parent;

        public CreateMessageViewModel(INavigation _navigation, CreateMessagePage parent)
        {
            Navigation = _navigation;
            PageTitle = "Poster un message";
            Parent = parent;
        }

        #region Properties

        private string _content;
        public string Content
        {
            get { return _content; }
            set
            {
                if (SetPropertyValue(ref _content, value))
                {
                    ((Command)CreateCommand).ChangeCanExecute();
                }
            }
        }

        #endregion


        #region Commands

        private ICommand _createCommand;
        public ICommand CreateCommand
        {
            get { return _createCommand = _createCommand ?? new Command(CreateAction, CanCreateAction); }
        }


        #endregion


        #region Methods

        bool CanCreateAction()
        {
            if (string.IsNullOrWhiteSpace(this.Content))
                return false;

            return true;
        }

        void CreateAction()
        {
            IsBusy = true;
            if (WSHelper.WSM2LinkClient.CreateMessage(AppConstants.User.Id, Content))
            {
                Content = "";
                Parent.DisplayAlert("Information", "Message posté", "Ok");
                IsBusy = false;
            }
            else
            {
                Parent.DisplayAlert("Erreur", "Erreur lors du post du message", "Ok");
                IsBusy = false;
            }
        }


        #endregion
    }
}
