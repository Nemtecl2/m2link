﻿using M2Link.Xamarin.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace M2Link.Xamarin.ViewModels
{
    public class UsersViewModel : BaseViewModel
    {
        private readonly INavigation Navigation;
        private readonly UsersPage Parent;

        public UsersViewModel(INavigation _navigation, UsersPage parent)
        {
            Navigation = _navigation;
            PageTitle = "Users";
            Parent = parent;
            var users = WSHelper.WSM2LinkClient.Users(AppConstants.User.Id).ToList();
            var list = users.Select(o => new UserType
            {
                Parent = Parent,
                Id = o.Id,
                Email = o.Email,
                Fullname = o.FirstName + " " + o.LastName + " (@" + o.Pseudo + ")",
                IsFollowed = o.IsFollowed,
                CreatedAt = o.CreatedAt,
                FollowState = o.IsFollowed.HasValue ? (o.IsFollowed.Value ? "Vous le suivez" : "Vous ne le suivez pas") : "C'est vous !",
                ImageUrl = Convert.FromBase64String(o.ImageUrl.Replace("data:image;base64,", "")),
            }).ToList();

            Users = new ObservableCollection<UserType>();
            foreach (var elem in list)
            {
                Users.Add(elem);
            }
        }

        #region Properties

        private ObservableCollection<UserType> _users;
        public ObservableCollection<UserType> Users
        {
            get { return _users; }
            set
            {
                SetPropertyValue(ref _users, value);

            }
        }

        
        #endregion
    }

    public class UserType
    {
        public UsersPage Parent { get; set; }
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Fullname { get; set; }
        public string Email { get; set; }
        public string FollowState { get; set; }
        public bool? IsFollowed { get; set; }
        public byte[] ImageUrl { get; set; }
        public string FollowLabel
        {
            get
            {
                return IsFollowed.HasValue ? (IsFollowed.Value ? "Ne plus suivre" : "Suivre") : "";
            }
        }

        public bool DisplayBtn
        {
            get
            {
                return IsFollowed.HasValue;
            }
        }

        private ICommand _buttonCommand;
        public ICommand ButtonCommand
        {
            get { return _buttonCommand = _buttonCommand ?? new Command(ButtonAction); }
        }

        void ButtonAction()
        {
            if (!IsFollowed.HasValue)
                return;
            if (IsFollowed.Value)
            {
                WSHelper.WSM2LinkClient.Unfollow(AppConstants.User.Id, Id);
                Parent.DisplayAlert("Information", "Vous ne le suivez plus", "Ok");
                Parent.Refresh();
            }
            else
            {
                WSHelper.WSM2LinkClient.Follow(AppConstants.User.Id, Id);
                Parent.DisplayAlert("Information", "Vous le suivez", "Ok");
                Parent.Refresh();
            }

        }
    }
}
