﻿using M2Link.Xamarin.Views;
using Xamarin.Forms;

namespace M2Link.Xamarin.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        private readonly INavigation Navigation;
        private readonly HomePage Parent;

        public HomeViewModel(INavigation _navigation, HomePage parent)
        {
            Navigation = _navigation;
            PageTitle = "Accueil";
            Parent = parent;
        }

        #region Properties

        #endregion
    }
}
