﻿using M2Link.Xamarin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2Link.Xamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UsersPage : ContentPage
    {
        public UsersPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        public void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            MessageType selectedItem = e.SelectedItem as MessageType;
        }

        public void OnListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            MessageType tappedItem = e.Item as MessageType;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refresh();
        }
        public void Refresh()
        {
            BindingContext = new UsersViewModel(Navigation, this);
        }
    }
}