﻿using M2Link.Xamarin.ViewModels;
using M2Link.Xamarin.WebServiceClients;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace M2Link.Xamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MessagesPage : ContentPage
    {        
        public MessagesPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        public void OnListViewItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            MessageType selectedItem = e.SelectedItem as MessageType;
        }

       public void OnListViewItemTapped(object sender, ItemTappedEventArgs e)
        {
            MessageType tappedItem = e.Item as MessageType;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            Refresh();
        }

        public void Refresh()
        {
            BindingContext = new MessagesViewModel(Navigation, this);
        }
    }
}