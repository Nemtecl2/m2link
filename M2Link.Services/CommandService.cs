﻿
using M2Link.Persistence;

namespace M2Link.Services
{
    /// <summary>
    /// Service abstrait pour la création, éditio ou suppression
    /// </summary>
    public abstract class CommandService
    {
        protected Transaction Transaction { get; private set; }

        public CommandService()
        {
            Transaction = new Transaction(new M2LinkContext());
        }
    }
}
