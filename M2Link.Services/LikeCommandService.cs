﻿using M2Link.Domain;
using M2Link.Repositories;
using M2Link.Services.Commands;
using M2Link.Services.Data;

namespace M2Link.Services
{
    public class LikeCommandService : CommandService
    {
        private readonly UserRepository _userRepository;
        private readonly MessageRepository _messageRepository;
        private readonly LikeRepository _likeRepository;
        public LikeCommandService()
        {
            _userRepository = new UserRepository(Transaction.Context);
            _messageRepository = new MessageRepository(Transaction.Context);
            _likeRepository = new LikeRepository(Transaction.Context);
        }

        public ResultData Create(LikeCommand cmd)
        {
            var message = _messageRepository.GetById(cmd.MessageId);
            using (Transaction)
            {
                if (_likeRepository.Exists(cmd.UserId, cmd.MessageId))
                {
                    return new ResultData
                    {
                        Success = false  
                    };
                }

                _likeRepository.Add(new Like
                {
                    Message = message,
                    User = _userRepository.GetById(cmd.UserId)
                });
            }

            return new ResultData
            {
                Success = true,
                Value = _messageRepository.GetById(cmd.MessageId).Likes.Count
            };
        }

        public ResultData Delete(LikeCommand cmd)
        {
            using (Transaction)
            {
                var like = _likeRepository.GetByUserAndMessageId(cmd.UserId, cmd.MessageId);
                if (like != null)
                {
                    _likeRepository.Delete(like);
                }
                else
                {
                    return new ResultData
                    {
                        Success = false
                    };
                }
            }

            return new ResultData
            {
                Success = true,
                Value = _messageRepository.GetById(cmd.MessageId).Likes.Count
            };
        }
    }
}
