﻿using M2Link.Domain;
using M2Link.Repositories;
using M2Link.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2Link.Services
{
    public class LikeQueryService : QueryService
    {
        private readonly UserRepository _userRepository;
        private readonly MessageRepository _messageRepository;
        private readonly LikeRepository _likeRepository;


        public LikeQueryService() : base()
        {
            _userRepository = new UserRepository(Context);
            _messageRepository = new MessageRepository(Context);
            _likeRepository = new LikeRepository(Context);
        }

        public List<LikeData> GetLikes(Guid messageId)
        {
            return _likeRepository.GetAll()
                        .Where(o => o.Message.Id == messageId)
                        .Select(o => new LikeData
                        {
                            Id = o.Id,
                            MessagesId = messageId,
                            UserFirstName = o.User.FirstName,
                            UserLastName = o.User.LastName,
                            UserId = o.User.Id,
							UserImageUrl = o.User.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(o.User.Image.Byte),
                            UserPseudo = o.User.Pseudo,
                            CreatedAt = o.CreatedAt
                        }).ToList();
        }
    }
}
