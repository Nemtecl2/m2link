﻿using M2Link.Domain;
using M2Link.Repositories;
using M2Link.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2Link.Services
{
    public class DocumentQueryService : QueryService
    {
        private readonly DocumentRepository _documentRepository;


        public DocumentQueryService() : base()
        {
            _documentRepository = new DocumentRepository(Context);
        }

		public DocumentData GetById(Guid id)
		{
			return _documentRepository.GetAll()
				.Select(o => new DocumentData
				{
					Id = o.Id,
					ContentType = o.ContentType,
					CreatedAt = o.CreatedAt,
					Length = o.Length,
					Byte = o.Byte
				})
				.FirstOrDefault(o => o.Id == id);
		}
      
    }
}
