﻿using System;

namespace M2Link.Services.Commands
{
	public class UserCommand
	{
		public Guid Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string Pseudo { get; set; }
		public string Password { get; set; }
		public string Description { get; set; }
		public byte[] Byte { get; set; }
		public string ContentType { get; set; }
    }
}
