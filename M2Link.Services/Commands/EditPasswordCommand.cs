﻿using System;

namespace M2Link.Services.Commands
{
	public class EditPasswordCommand
	{
		public Guid Id { get; set; }
		public string Password { get; set; }
    }
}
