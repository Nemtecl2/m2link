﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2Link.Services.Commands
{
    public class MessageCommand
    {
        public Guid UserId { get; set; }
        public string Content { get; set; }
    }
}
