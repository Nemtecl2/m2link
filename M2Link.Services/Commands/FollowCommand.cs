﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2Link.Services.Commands
{
    public class FollowCommand
    {
        public Guid UserId { get; set; }
        public Guid SubscriptionId { get; set; }
    }
}
