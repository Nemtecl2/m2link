﻿using System.Collections.Generic;

namespace M2Link.Services.Data
{
    public class SimpleUserData : EntityData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Pseudo { get; set; }
        public string Description { get; set; }
        public string ImageUrl{ get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }


        public SimpleUserData() : base()
        {
        }
    }
}