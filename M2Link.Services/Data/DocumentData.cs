﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2Link.Services.Data
{
	public class DocumentData : EntityData
	{
		public byte[] Byte { get; set; }
		public long Length { get; set; }
		public string ContentType { get; set; }
	}
}
