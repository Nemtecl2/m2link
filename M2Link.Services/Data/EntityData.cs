﻿using System;
using System.Collections.Generic;
using System.Text;

namespace M2Link.Services.Data
{
    /// <summary>
    /// Data pour une Entity
    /// </summary>
    public class EntityData
    {
        public Guid Id { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
