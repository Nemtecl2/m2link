﻿using System;
using System.Collections.Generic;

namespace M2Link.Services.Data
{
    public class MessagesData
    {
        public List<MessageData> Messages { get; set; }

        public MessagesData()
        {
            Messages = new List<MessageData>();
        }
    }

    public class MessageData : EntityData
    {
        public string Content { get; set; }
        public Guid UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserPseudo { get; set; }
        public string ImageUrl { get; set; }
        public List<LikeData> Likes { get; set; }
        public bool IsLiked { get; set; }

        public MessageData()
        {
            Likes = new List<LikeData>();
        }
    }
}