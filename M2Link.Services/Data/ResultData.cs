﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2Link.Services.Data
{
    public class ResultData
    {
        public bool Success { get; set; }
        public int Value { get; set; }
    }
}
