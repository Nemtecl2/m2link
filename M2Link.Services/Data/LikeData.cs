﻿using M2Link.Domain;
using System;

namespace M2Link.Services.Data
{
    public class LikeData : Entity
    {
        public Guid MessagesId { get; set; }
        public Guid UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserPseudo { get; set; }
        public string UserImageUrl { get; set; }
    }
}