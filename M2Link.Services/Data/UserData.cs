﻿using System.Collections.Generic;

namespace M2Link.Services.Data
{
    public class UserData : SimpleUserData
    {
        public MessagesData Messages { get; set; }        
        public List<FollowableData> Subscriptions { get; set; }
        public bool? IsFollowed { get; set; }

        public UserData() : base()
        {
            Subscriptions = new List<FollowableData>();
        }
    }
}