﻿namespace M2Link.Services.Data
{
    public class FollowableData : EntityData
    {
        public string Pseudo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool? IsFollowed { get; set; }
        public string ImageUrl { get; set; }
    }
}