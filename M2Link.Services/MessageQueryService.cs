﻿using M2Link.Domain;
using M2Link.Repositories;
using M2Link.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2Link.Services
{
    public class MessageQueryService : QueryService
    {
        private readonly UserRepository _userRepository;
        private readonly MessageRepository _messageRepository;
        private readonly LikeRepository _likeRepository;


        public MessageQueryService() : base()
        {
            _userRepository = new UserRepository(Context);
            _messageRepository = new MessageRepository(Context);
            _likeRepository = new LikeRepository(Context);
        }


        public MessagesData GetTimeline(Guid currentUserId)
        {

            var currentUser = _userRepository.GetById(currentUserId);
            return new MessagesData
            {
                // On ajoute l'utilisateur courrant poour qu'il puisse voir ses propres tweets 
                Messages = (from m in _messageRepository.GetAll()
                            where m.User.Id == currentUserId || currentUser.Subscriptions.Any(o => o.Id == m.User.Id)
                            select new MessageData
                            {
                                Id = m.Id,
                                CreatedAt = m.CreatedAt,
                                Content = m.Content,
                                ImageUrl = m.User.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(m.User.Image.Byte),
                                UserId = m.User.Id,
                                UserPseudo = m.User.Pseudo,
                                UserFirstName = m.User.FirstName,
                                UserLastName = m.User.LastName,
                                IsLiked = _likeRepository.GetAll().Any(o => o.User.Id == currentUserId && o.Message.Id == m.Id),
                                Likes = (from l in m.Likes
                                         select new LikeData
                                         {
                                             Id = l.Id,
                                             MessagesId = m.Id,
                                             UserId = l.User.Id,
                                             UserFirstName = l.User.FirstName,
                                             UserLastName = l.User.LastName,
                                             UserPseudo = l.User.Pseudo,
                                             UserImageUrl = l.User.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(l.User.Image.Byte),
											 CreatedAt = l.CreatedAt
                                         }).ToList()
                            })
                            .OrderByDescending(o => o.CreatedAt)
                            .ToList()
            };
        }

        public MessagesData GetUserMessages(Guid userId, Guid currentUserId)
        {
            var currentUser = _userRepository.GetById(userId);
            return new MessagesData
            {
                Messages = (from m in _messageRepository.GetAll()
                            where m.User.Id == userId
                            select GetById(m.Id, currentUserId))
                            .OrderByDescending(o => o.CreatedAt)
                            .ToList()
            };
        }

        public MessageData GetById(Guid messageId, Guid currentUserId)
        {
            return _messageRepository
                .GetAll()
                .Select(m => new MessageData
                {
                    Id = m.Id,
                    CreatedAt = m.CreatedAt,
                    Content = m.Content,
                    ImageUrl = m.User.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(m.User.Image.Byte),
					UserId = m.User.Id,
                    UserPseudo = m.User.Pseudo,
                    UserFirstName = m.User.FirstName,
                    UserLastName = m.User.LastName,
                    IsLiked = _likeRepository.GetAll().Any(o => o.User.Id == currentUserId && o.Message.Id == m.Id),
                    Likes = (from l in m.Likes
                             select new LikeData
                             {
                                 Id = l.Id,
                                 MessagesId = m.Id,
                                 UserId = l.User.Id,
                                 UserFirstName = l.User.FirstName,
                                 UserLastName = l.User.LastName,
                                 UserPseudo = l.User.Pseudo,
                                 UserImageUrl = l.User.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(l.User.Image.Byte),
                                 CreatedAt = l.CreatedAt
                             }).ToList()
                })
                .FirstOrDefault(o => o.Id == messageId);
        }
    }
}
