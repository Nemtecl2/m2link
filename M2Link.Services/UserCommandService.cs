﻿using M2Link.Domain;
using M2Link.Repositories;
using M2Link.Services.Commands;
using M2Link.Services.Util;

namespace M2Link.Services
{
    public class UserCommandService : CommandService
    {
        private readonly UserRepository _userRepository;
        public UserCommandService()
        {
            _userRepository = new UserRepository(Transaction.Context);
        }
        
        public void Create(UserCommand cmd)
        {
            using (Transaction)
            {
				Document image = null; 
				if (cmd.Byte != null)
				{
					image = new Document
					{
						Byte = cmd.Byte,
						ContentType = cmd.ContentType,
						Length = cmd.Byte.Length
					};
				}

                byte[] passwordHash, passwordSalt;
                PasswordManager.CreatePasswordHash(cmd.Password, out passwordHash, out passwordSalt);

                _userRepository.Add(new User
                {
                    FirstName = cmd.FirstName,
                    LastName = cmd.LastName,
                    Email = cmd.Email,
                    Pseudo = cmd.Pseudo,
                    PasswordHash = passwordHash,
                    PasswordSalt = passwordSalt,
                    Description = cmd.Description,
                    Image = image
                });
            }
        }

        public void Edit(UserCommand cmd)
        {

            using (Transaction)
            {
                var user = _userRepository.GetById(cmd.Id);
				// On recupère l'image courrante
				Document document = user.Image;
				// Si on change l'image
				if (cmd.Byte != null)
				{
					// Si l'utilisateur a pas d'image on lui en créé une
					if (user.Image == null)
					{
						document = new Document
						{
							Byte = cmd.Byte,
							ContentType = cmd.ContentType,
							Length = cmd.Byte.Length
						};
					}
					else // Sinon on la change
					{
						document.ContentType = cmd.ContentType;
						document.Byte = cmd.Byte;
						document.Length = cmd.Byte.Length;
					}
				}
                user.Update(cmd.FirstName, cmd.LastName, cmd.Email, cmd.Pseudo, cmd.Description, document);
            }
        }

        public string Follow(FollowCommand cmd)
        {
            var subscription = _userRepository.GetById(cmd.SubscriptionId);
            using (Transaction)
            {
                _userRepository.GetById(cmd.UserId).Follow(subscription);
            }

            return subscription.Pseudo;
        }

        public string Unfollow(FollowCommand cmd)
        {
            var subscription = _userRepository.GetById(cmd.SubscriptionId);
            using (Transaction)
            {
                _userRepository.GetById(cmd.UserId).Unfollow(subscription);
            }

            return subscription.Pseudo;
        }
    }
}
