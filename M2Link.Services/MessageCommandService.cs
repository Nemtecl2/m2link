﻿using M2Link.Domain;
using M2Link.Repositories;
using M2Link.Services.Commands;

namespace M2Link.Services
{
    public class MessageCommandService : CommandService
    {
        private readonly UserRepository _userRepository;
        private readonly MessageRepository _messageRepository;
        public MessageCommandService()
        {
            _userRepository = new UserRepository(Transaction.Context);
            _messageRepository = new MessageRepository(Transaction.Context);
        }

        public void Create(MessageCommand cmd)
        {
            using (Transaction)
            {
                var user = _userRepository.GetById(cmd.UserId);
                _messageRepository.Add(new Message
                {
                    User = user,
                    Content = cmd.Content
                });
            }
        }
    }
}
