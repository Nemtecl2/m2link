﻿
using M2Link.Persistence;

namespace M2Link.Services
{
 
    /// <summary>
    /// Service abstrait pour les requêtes
    /// </summary>
    public abstract class QueryService
    {
        /// <summary>
        /// Contexte de l'application
        /// </summary>
        protected M2LinkContext Context { get; private set; }

        public QueryService()
        {
            Context = new M2LinkContext();
        }
    }
}
