﻿using M2Link.Domain;
using M2Link.Repositories;
using M2Link.Services.Commands;
using M2Link.Services.Data;
using System;

namespace M2Link.Services
{
    public class DocumentCommandService : CommandService
    {
        private readonly DocumentRepository _documentRepository;
        public DocumentCommandService()
        {
			_documentRepository = new DocumentRepository(Transaction.Context);
        }


        public void Delete(Guid id)
        {
			using (Transaction)
			{
				_documentRepository.Delete(_documentRepository.GetById(id));
			}
        }
    }
}
