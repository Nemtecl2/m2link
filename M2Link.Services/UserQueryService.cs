﻿using M2Link.Domain;
using M2Link.Domain.Util;
using M2Link.Repositories;
using M2Link.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace M2Link.Services
{
    public class UserQueryService : QueryService
    {
        private readonly UserRepository _userRepository;
        private readonly LikeRepository _likeRepository;
        public UserQueryService() : base()
        {
            _userRepository = new UserRepository(Context);
            _likeRepository = new LikeRepository(Context);
        }

        /// <summary>
        /// Récupère la liste des users
        /// </summary>
        /// <returns></returns>
        public List<SimpleUserData> GetAll()
        {
            return _userRepository.GetAll().Select(o => new SimpleUserData
            {
                Id = o.Id,
                FirstName = o.FirstName,
                LastName = o.LastName,
                Pseudo = o.Pseudo,
                Email = o.Email,
                Description = o.Description,
				ImageUrl = o.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(o.Image.Byte),
				CreatedAt = o.CreatedAt,
                PasswordHash = o.PasswordHash,
                PasswordSalt = o.PasswordSalt
            }).ToList();
        }

        /// <summary>
        /// Récupère l'utilisateur avec son id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SimpleUserData GetById(Guid id)
        {
            return GetAll().FirstOrDefault(o => o.Id == id);
        }

        public bool CorrectPassword(string tryPseudo, byte[] hash, byte[] salt)
        {
            return PasswordManager.VerifyPasswordHash(tryPseudo, hash, salt);
        }

        /// <summary>
        /// Récupère l'utilisateur par pseudo
        /// </summary>
        /// <param name="pseudo"></param>
        /// <returns></returns>
        public SimpleUserData GetByPseudo(string pseudo)
        {
            return GetAll().FirstOrDefault(o => o.Pseudo == pseudo);
        }

        /// <summary>
        /// Retourne la liste des utilisateurs avec un flag indiquant si l'utilisateur
        /// d'ID userId follow ou non utilisateur
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<FollowableData> GetFollowedAndFollowable(Guid userId)
        {

            return _userRepository.GetAll().Select(u => new FollowableData
            {
                Id = u.Id,
                FirstName = u.FirstName,
                LastName = u.LastName,
                Pseudo = u.Pseudo,
                Email = u.Email,
                CreatedAt = u.CreatedAt,
                IsFollowed = IsFollowing(userId, u.Id),
                ImageUrl = u.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(u.Image.Byte)
            }).ToList();
        }

        /// <summary>
        /// Retourne les détails de l'utilisateur userId
        /// Le currentUserId permet de déterminer si l'utilisateur actuellement authentifié
        /// est abonné à cet utilisateur
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserData GetUserDetails(Guid currentUserId, Guid userId)
        {
            var user = _userRepository.GetById(userId);

			return new UserData
			{
				Id = userId,
				FirstName = user.FirstName,
				LastName = user.LastName,
				Pseudo = user.Pseudo,
				Email = user.Email,
				Description = user.Description,
				ImageUrl = user.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(user.Image.Byte),
				Messages = new MessagesData()
                {
                    Messages = user.Messages.Select(m => new MessageData
                    {
                        Id = m.Id,
                        Content = m.Content,
                        CreatedAt = m.CreatedAt,
                        UserId = userId,
                        UserFirstName = user.FirstName,
                        UserLastName = user.LastName,
                        UserPseudo = user.Pseudo,
                        ImageUrl = user.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(user.Image.Byte),
						IsLiked = _likeRepository.GetAll().Any(o => o.User.Id == currentUserId && o.Message.Id == m.Id),
                        Likes = (from l in m.Likes
                                 select new LikeData
                                 {
                                     Id = l.Id,
                                     MessagesId = m.Id,
                                     UserId = l.User.Id,
                                     UserFirstName = l.User.FirstName,
                                     UserLastName = l.User.LastName,
                                     UserPseudo = l.User.Pseudo,
                                     UserImageUrl = l.User.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(l.User.Image.Byte),
                                     CreatedAt = l.CreatedAt
                                 }).ToList()
                    }).ToList()
                },
                Subscriptions = user.Subscriptions.Select(m => new FollowableData
                {
                    Id = m.Id,
                    FirstName = m.FirstName,
                    LastName = m.LastName,
                    IsFollowed = true,
                    Pseudo = m.Pseudo,
                    Email = m.Email,
                    CreatedAt = m.CreatedAt,
                    ImageUrl = m.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(m.Image.Byte),
				}).ToList(),
                CreatedAt = user.CreatedAt,
                IsFollowed = IsFollowing(currentUserId, userId)
            };
        }

        /// <summary>
        /// Verifie si l'utilisateur existe
        /// </summary>
        /// <param name="pseudo"></param>
        /// <returns></returns>
        public bool Exists(string pseudo)
        {
            return _userRepository.GetAll().Any(o => o.Pseudo == pseudo);
        }

        /// <summary>
        /// Vérifie si l'utilisateur courrant suit l'utilisateur subscription
        /// </summary>
        /// <param name="currentUserId"></param>
        /// <param name="subscriptionId"></param>
        /// <returns></returns>
        public bool? IsFollowing(Guid currentUserId, Guid subscriptionId)
        {
            return currentUserId != subscriptionId ? 
                (_userRepository.GetById(currentUserId).Subscriptions.Any(o => o.Id == subscriptionId) ? true : false) : new bool?();
        }
    }
}
