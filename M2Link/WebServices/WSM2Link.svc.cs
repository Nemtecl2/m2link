﻿using M2Link.Services;
using M2Link.Services.Commands;
using M2Link.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace M2Link.WebSite.WebServices
{
    public class WSM2Link : IWSM2Link
    {
        private readonly UserQueryService _userQueryService;
        private readonly MessageQueryService _messageQueryService;
        private readonly MessageCommandService _messageCommandService;
        private readonly UserCommandService _userCommandService;
        private readonly LikeCommandService _likeCommandService;


        public WSM2Link()
        {
            _userQueryService = new UserQueryService();
            _messageQueryService = new MessageQueryService();
            _messageCommandService = new MessageCommandService();
            _userCommandService = new UserCommandService();
            _likeCommandService = new LikeCommandService();
        }

        public bool CreateMessage(Guid currentUserId, string content)
        {
            if (content == null || content.Length == 0)
            {
                return false;
            }
            else
            {
                _messageCommandService.Create(new MessageCommand
                {
                    Content = content,
                    UserId = currentUserId
                });
            }
            return true;
        }

        public bool Dislike(Guid currentUserId, Guid messageId)
        {
            var result = _likeCommandService.Delete(new LikeCommand
            {
                MessageId = messageId,
                UserId = currentUserId
            });

            return result.Success;
        }

        public string Follow(Guid currentUserId, Guid userId)
        {
            return _userCommandService.Follow(new FollowCommand
            {
                UserId = currentUserId,
                SubscriptionId = userId
            });
        }

        public SimpleUserData GetUser(Guid userId, Guid currentUserId)
        {
            return _userQueryService.GetUserDetails(currentUserId, userId);
        }

        public bool Like(Guid currentUserId, Guid messageId)
        {
            var result = _likeCommandService.Create(new LikeCommand
            {
                MessageId = messageId,
                UserId = currentUserId
            });

            return result.Success;
        }

        public string Register(string firstname, string lastname, string email, string pseudo, string password, string description)
        {
            if (_userQueryService.Exists(pseudo))
            {
                return "L'utilisateur existe déjà";
            }

            try
            {
                _userCommandService.Create(new UserCommand
                {
                    FirstName = firstname,
                    LastName = lastname,
                    Email = email,
                    Pseudo = pseudo,
                    Password = password,
                    Description = description,
                });
            }
            catch
            {
                return "Problème lors de la création de l'utilisateur";
            }
            return null;
        }

        public MessagesData Timeline(Guid currentUserId)
        {
            return _messageQueryService.GetTimeline(currentUserId);
        }

        public string Unfollow(Guid currentUserId, Guid userId)
        {
            var pseudo = _userCommandService.Unfollow(new FollowCommand
            {
                UserId = currentUserId,
                SubscriptionId = userId
            });

            return pseudo;
        }

        public List<FollowableData> Users(Guid currentUserId)
        {
            return _userQueryService.GetFollowedAndFollowable(currentUserId);
        }

        public SimpleUserData Validate(string pseudo, string password)
        {
            var user = _userQueryService.GetByPseudo(pseudo);
            if (!_userQueryService.CorrectPassword(password, user.PasswordHash, user.PasswordSalt))
            {
                return null;
            }
            return user;
        }
    }
}
