﻿using M2Link.Services.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace M2Link.WebSite.WebServices
{
    // REMARQUE : vous pouvez utiliser la commande Renommer du menu Refactoriser pour changer le nom d'interface "IWSM2Link" à la fois dans le code et le fichier de configuration.
    [ServiceContract]
    public interface IWSM2Link
    {
        [OperationContract]
        SimpleUserData Validate(string pseudo, string password);
        [OperationContract]
        string Register(string firstname, string lastname, string email, string pseudo, string password, string description);
        [OperationContract]
        MessagesData Timeline(Guid currentUserId);
        [OperationContract]
        List<FollowableData> Users(Guid currentUserId);
        [OperationContract]
        SimpleUserData GetUser(Guid userId, Guid currentUserId);
        [OperationContract]
        bool CreateMessage(Guid currentUserId, string content);
        [OperationContract]
        string Follow(Guid currentUserId, Guid userId);
        [OperationContract]
        string Unfollow(Guid currentUserId, Guid userId);
        [OperationContract]
        bool Like(Guid currentUserId, Guid messageId);
        [OperationContract]
        bool Dislike(Guid currentUserId, Guid messageId);
    }
}
