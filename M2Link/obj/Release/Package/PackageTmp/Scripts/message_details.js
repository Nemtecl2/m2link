﻿$(document).ready(function () {
    initMessageLike();
    createUserLike();
});

function like(id, el) {
    $.ajax({
        type: 'POST',
        url: '/Messages/Like',
        dataType: 'json',
        data: { messageId: id },
        success: function (data) {
            if (data.Success) {
                $(el).addClass('liked');
                $(el).siblings('.counter').text(data.Count);
                createUserLike();
                initMessageLike();
            } else {
                toastr.error("Vous aimez déjà ce tweet", null, toastrOptions());
            }
        },
        error: function (ex) {
            console.error(ex);
        }
    });
    return false;
}

function dislike(id, el) {
    $.ajax({
        type: 'POST',
        url: '/Messages/Dislike',
        dataType: 'json',
        data: { messageId: id },
        success: function (data) {
            if (data.Success) {
                $(el).removeClass('liked');
                $(el).siblings('.counter').text(data.Count);
                createUserLike();
                initMessageLike();
            } else {
                toastr.error("Vous n'aimez déjà pas ce tweet", null, toastrOptions());
            }
        },
        error: function (ex) {
            console.error(ex);
        }
    });
    return false;
}

function createUserLike() {
    var messageId = $('input[type=hidden]').val();
    $.ajax({
        type: 'GET',
        url: '/Messages/GetLikes',
        data: { messageId },
        dataType: 'json',
        success: function (data) {
            buildUserMessages(data.Data);
        },
        error: function (ex) {
            console.error(ex);
        }
    });
    return false;
}

function buildUserMessages(data) {
    $('div.user-liked-container').empty();
    let elements = '';
    data.map((like) => {
        let messageDate = like.CreatedAt;
        let dateString = messageDate.slice(messageDate.lastIndexOf("(") + 1, messageDate.lastIndexOf(")"));
        let date = new Date(parseInt(dateString, 10));
        let noImage = '';
        elements += `
		<div class="card-user">
            <div class="card-user-image">
                <img class="rounded-messages" src="${like.UserImageUrl ? like.UserImageUrl : noImage}" alt="profile picture" />
            </div>
            <div class="card-user-info">
                <span class="card-user-username">${like.UserFirstName + ' ' + like.UserLastName + ' ('}<a href="/Users/Details/${like.UserId}">@${like.UserPseudo}</a>${')'}</span>
                <span class="card-user-date">${date.toLocaleDateString() + ' à ' + date.toLocaleTimeString()}</span>
            </div>
        </div>`;
    });
    $('div.user-liked-container').append(elements);
    
}

function initMessageLike() {
    $('.like:not(.liked)')
        .off('click')
        .on('click', function (e) {
            e.stopPropagation();
            var id = $(this).siblings('input[type=hidden]').val();
            like(id, this);
        });

    $('.like.liked')
        .off('click')
        .on('click', function (e) {
            e.stopPropagation();
            var id = $(this).siblings('input[type=hidden]').val();
            dislike(id, this);
        });
}