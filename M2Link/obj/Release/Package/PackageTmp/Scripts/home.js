﻿$(document).ready(function () {
	$('[data-toggle="tooltip"]').tooltip();
	$('#publish').on('click', function () {
		$('.timeline-container').addClass('loading');
		$.ajax({
			type: 'post',
			url: '/Messages/Create',
			data: {
				content: $('#create-area').val()
			},
			success: function (result) {
				if (result.result) {
					$('#create-area').val("");
					createTimeline();
				} else {
					$('.timeline-container').removeClass('loading');
					toastr.error('Un message doit contenir du texte', null, toastrOptions());
				}
			}
		});
	});
	createUserAvatar();
	createTimeline();
});

function createUserAvatar() {
	$.ajax({
		type: 'GET',
		url: '/Users/AvatarUrl',
		dataType: 'json',
		success: function (data) {
			if (data.ImageUrl)
				$('.user-message-create-img').prop('src', data.ImageUrl);
		},
		error: function (ex) {
			console.error(ex);
		}
	});
	return false;
}

function createTimeline() {
	$.ajax({
		type: 'GET',
		url: '/Messages/Timeline',
		dataType: 'json',
		success: function (data) {
			$('.timeline-container').removeClass('loading');
			buildMessages(data.Data);
			$('[data-toggle="tooltip"]').tooltip();
			initMessages();
		},
		error: function (ex) {
			console.error(ex);
		}
	});
	return false;
}