﻿$(function () {
    initUser();
    createUserMessages();
});

function createUserMessages() {
    var userId = $('input[type=hidden]').val();
    $.ajax({
        type: 'GET',
        url: '/Messages/UserMessages',
		data: { userId },
        dataType: 'json',
        success: function (data) {
            buildMessages(data.Data);
            $('[data-toggle="tooltip"]').tooltip();
            initMessages();
        },
        error: function (ex) {
            console.error(ex);
        }
    });
    return false;
}

function follow(id, el) {
    $.ajax({
        type: 'POST',
        url: '/Users/Follow',
        dataType: 'json',
        data: { subscriptionId: id },
        success: function (data) {
            $(el).addClass('followed');
            toastr.success(data.Message, null, toastrOptions());
            initUser();
        },
        error: function (ex) {
            console.error(ex);
            toastr.error("Erreur lors de la tentative de désabonnement", null, toastrOptions());
        }
    });
    return false;
}

function unfollow(id, el) {
    $.ajax({
        type: 'POST',
        url: '/Users/Unfollow',
        dataType: 'json',
        data: { subscriptionId: id },
        success: function (data) {
            $(el).removeClass('followed');
            toastr.success(data.Message, null, toastrOptions());
            initUser();
        },
        error: function (ex) {
            console.error(ex);
            toastr.error("Erreur lors de la tentative d'abonnement", null, toastrOptions());
        }
    });
    return false;
}

function initUser() {
    $('.followable:not(.followed)')
        .off('click')
        .on('click', function () {
            var id = $('input[type=hidden]').val();
            follow(id, this);
        });

    $('.followable.followed')
        .off('click')
        .on('click', function () {
            var id = $('input[type=hidden]').val();
            unfollow(id, this);
        });
}
