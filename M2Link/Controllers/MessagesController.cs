﻿using M2Link.Services;
using M2Link.Services.Commands;
using M2Link.WebSite.ViewModels;
using System;
using System.Web.Mvc;

namespace M2Link.WebSite.Controllers
{
    public class MessagesController : M2LinkController
    {
        private readonly LikeCommandService _likeCommandService;
        private readonly LikeQueryService _likeQueryService;
        private readonly MessageCommandService _messageCommandService;
        private readonly MessageQueryService _messageQueryService;
        public MessagesController()
        {
            _likeCommandService = new LikeCommandService();
            _likeQueryService = new LikeQueryService();
            _messageCommandService = new MessageCommandService();
            _messageQueryService = new MessageQueryService();
        }

        [HttpGet]
        public ActionResult Details(Guid id)
        {
            return View(DataToModel(_messageQueryService.GetById(id, UserId)));
        }

        [HttpPost]
        public ActionResult Create(string content)
        {
            var result = true;
            if (content == null || content.Length == 0)
            {
                result = false;
            }
            else
            {
                _messageCommandService.Create(new MessageCommand
                {
                    Content = content,
                    UserId = UserId
                });
            }

            return Json(new { result });
        }

        [HttpGet]
        public ActionResult GetLikes(Guid messageId)
        {
            return Json(new
            {
                Data = _likeQueryService.GetLikes(messageId)
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Like(Guid messageId)
        {

            var result = _likeCommandService.Create(new LikeCommand
            {
                MessageId = messageId,
                UserId = UserId
            });

            return Json(new 
            { 
                result.Success,
                Count = result.Value
            });
        }

        [HttpPost]
        public ActionResult Dislike(Guid messageId)
        {
            var result = _likeCommandService.Delete(new LikeCommand
            {
                MessageId = messageId,
                UserId = UserId
            });

            return Json(new 
            { 
                result.Success,
                Count = result.Value
            });
        }

        public ActionResult Timeline()
        {
            return Json(new { Data = DataToModel(_messageQueryService.GetTimeline(UserId)) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserMessages(Guid userId)
        {
			return Json(new { Data = DataToModel(_messageQueryService.GetUserMessages(userId, UserId)) }, JsonRequestBehavior.AllowGet);
        }
    }
}