﻿using M2Link.Services;
using System.Web.Mvc;

namespace M2Link.WebSite.Controllers
{
  
    public class HomeController : M2LinkController
    {
        private readonly MessageQueryService _messageQueryService;
        public HomeController() : base()
        {
            _messageQueryService = new MessageQueryService();
        }

        public ActionResult Index()
        {
            return View();
        }

		[AllowAnonymous]
		public ActionResult Help()
		{
			return View();
		}
    }
}