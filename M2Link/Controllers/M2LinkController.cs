﻿using M2Link.Services.Data;
using M2Link.WebSite.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace M2Link.WebSite.Controllers
{
    [Authorize]
    public class M2LinkController : Controller
    {
        protected Guid UserId
        {
            get
            {
                return Guid.Parse(User.Identity.Name);
            }
        }

        public EditUserModel DataToModel(SimpleUserData data)
        {
            return new EditUserModel
            {
                FirstName = data.FirstName,
                LastName = data.LastName,
                Email = data.Email,
                Pseudo = data.Pseudo,
                Description = data.Description,
                ImageUrl = data.ImageUrl
            };
        }

        public MessagesModel DataToModel(MessagesData data)
        {
            return new MessagesModel
            {
                Messages = (from m in data.Messages
                            select DataToModel(m)).ToList()
            };
        }

        public MessageModel DataToModel(MessageData data)
        {
            return new MessageModel
            {
                Id = data.Id,
                Content = data.Content,
                ImageUrl = data.ImageUrl,
                UserId = data.UserId,
                UserFirstName = data.UserFirstName,
                UserLastName = data.UserLastName,
                UserPseudo = data.UserPseudo,
                CreatedAt = data.CreatedAt,
                IsLiked = data.IsLiked,
                Likes = (from l in data.Likes
                         select new LikeModel
                         {
                             Id = l.Id,
                             MessagesId = l.MessagesId,
                             UserId = l.UserId,
                             UserFirstName = l.UserFirstName,
                             UserLastName = l.UserLastName,
                             UserPseudo = l.UserPseudo,
                             UserImageUrl = l.UserImageUrl,
                             CreatedAt = l.CreatedAt
                         }).ToList()
            };
        }

        public List<FollowableModel> DataToModel(List<FollowableData> data)
        {

            return (from f in data
                    select new FollowableModel
                    {
                        Id = f.Id,
                        Email = f.Email,
                        Pseudo = f.Pseudo,
                        IsFollowed = f.IsFollowed,
                        FirstName = f.FirstName,
                        LastName = f.LastName,
                        CreatedAt = f.CreatedAt,
                        ImageUrl = f.ImageUrl,
                    }).ToList();
        }

        public UserModel DataToModel(UserData data)
        {
            return new UserModel
            {
                Id = data.Id,
                Email = data.Email,
                Pseudo = data.Pseudo,
                IsFollowed = data.IsFollowed,
                FirstName = data.FirstName,
                LastName = data.LastName,
                CreatedAt = data.CreatedAt,
                ImageUrl = data.ImageUrl,
                Description = data.Description,
                Messages = DataToModel(data.Messages),
                Subscriptions = DataToModel(data.Subscriptions)
            };
        }
    }
}