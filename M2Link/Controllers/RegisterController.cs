﻿using M2Link.Repositories;
using M2Link.Services;
using M2Link.Services.Commands;
using M2Link.WebSite.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace M2Link.WebSite.Controllers
{
    public class RegisterController : M2LinkController
    {
        private readonly UserQueryService _userQueryService;
        private readonly UserCommandService _userCommandService;
        public RegisterController() : base()
        {
            _userQueryService = new UserQueryService();
            _userCommandService = new UserCommandService();
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Form()
        {
            return View(new RegisterModel());
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Form(RegisterModel model)
        {
         
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (_userQueryService.Exists(model.Pseudo))
            {
                ModelState.AddModelError("Pseudo", "Cet utilisateur existe déjà");
                return View(model);
            }

            try
            {
				byte[] img = null;
                if (model.File != null && model.File.ContentLength > 0)
                {
					Stream fs = model.File.InputStream;
					fs.Position = 0;
					BinaryReader br = new BinaryReader(fs);
					img = br.ReadBytes((Int32)fs.Length);
				}

				

                _userCommandService.Create(new UserCommand
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Email = model.Email,
                    Pseudo = model.Pseudo,
                    Password = model.Password,
                    Description = model.Description,
					Byte = img,
					ContentType = model.File?.ContentType
                });
            }
            catch
            {
                ModelState.AddModelError("File", "Erreur lors de l'upload");
                return View(model);
            }

            return RedirectToAction("Index", "Home");
        }
    }
}