﻿using M2Link.WebSite.ViewModels;
using System;
using System.Configuration;
using System.IO;
using System.Web.Mvc;
using System.Web.Security;
using M2Link.Repositories;
using M2Link.Services;
using M2Link.Services.Commands;
using M2Link.Services.Data;

namespace M2Link.WebSite.Controllers
{
    public class UsersController : M2LinkController
    {
        private readonly UserQueryService _userQueryService;
        private readonly UserCommandService _userCommandService;
        public UsersController() : base()
        {
            _userQueryService = new UserQueryService();
            _userCommandService = new UserCommandService();
        }

        // GET: Index

        // Recupère la liste des users
        [HttpGet]
        public ActionResult Index()
        {
            return View(DataToModel(_userQueryService.GetFollowedAndFollowable(UserId)));
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = _userQueryService.GetByPseudo(model.Pseudo);
                if (user == null)
                {
                    ModelState.AddModelError("Pseudo", "Cet utilisateur n'existe pas");
                }
                else if (!_userQueryService.CorrectPassword(model.Password, user.PasswordHash, user.PasswordSalt))
                {
                    ModelState.AddModelError("Password", "Mot de passe incorect");
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(user.Id.ToString(), false);
                    return RedirectToAction("Index", "Home");
                }
            }

            return View("Login", model);
        }

        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return Json(new { Url = "/Home/Index" });
        }

        [HttpGet]
        public ActionResult Edit()
        {
            return View(DataToModel(_userQueryService.GetById(UserId)));
        }

        [HttpPost]
        public ActionResult Edit(EditUserModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
				byte[] img = null;
				if (model.File != null && model.File.ContentLength > 0)
				{
						Stream fs = model.File.InputStream;
						fs.Position = 0;
						BinaryReader br = new BinaryReader(fs);
						img = br.ReadBytes((Int32)fs.Length);
				}

				_userCommandService.Edit(new UserCommand
                {
                    Id = UserId,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Pseudo = model.Pseudo,
                    Email = model.Email,
                    Description = model.Description,
					Byte = img,
					ContentType = model.File?.ContentType
				});
            }
            catch
            {
                ModelState.AddModelError("File", "Erreur lors de l'upload");
                return View(model);
            }


            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Details(Guid id)
        {
            return View(DataToModel(_userQueryService.GetUserDetails(UserId, id)));
        }


        [HttpPost]
        public ActionResult Follow(Guid subscriptionId)
        {
            var pseudo = _userCommandService.Follow(new FollowCommand
            {
                UserId = UserId,
                SubscriptionId = subscriptionId
            });

            return Json(new { Message = "Vous venez de suivre " + pseudo });
        }

        [HttpPost]
        public ActionResult Unfollow(Guid subscriptionId)
        {
            var pseudo = _userCommandService.Unfollow(new FollowCommand
            {
                UserId = UserId,
                SubscriptionId = subscriptionId
            });

            return Json(new { Message = "Vous ne suivez plus " + pseudo });
        }

        public ActionResult AvatarUrl()
        {
			return Json(new { _userQueryService.GetById(UserId).ImageUrl }, JsonRequestBehavior.AllowGet);
        }

    }
}