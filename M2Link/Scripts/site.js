﻿$(function () {
    $('#logOut').on('click', function () {
        $.ajax({
            type: 'post',
            url: '/Users/Logout',
            success: function (result) {
                window.location.href = result.Url;
            }
        });
    });

	$('[data-toggle="tooltip"]').tooltip();
})



function toastrOptions() {
    return {
        closeButton: true,
        newestOnTop: true,
        progressBar: true,
        positionClass: "toast-top-right",
        showDuration: "300",
        hideDuration: "1000",
        timeOut: "3000",
        extendedTimeOut: "1000",
        showEasing: "swing",
        hideEasing: "linear",
        showMethod: "fadeIn",
        hideMethod: "fadeOut"
    }
}

function buildMessages(data) {
	let datas = data.Messages;
	$('div.timeline').empty();
	let elements = '';
	datas.map((message) => {
		let messageDate = message.CreatedAt;
		let dateString = messageDate.slice(messageDate.lastIndexOf("(") + 1, messageDate.lastIndexOf(")"));
		let date = new Date(parseInt(dateString, 10));
		let noImage = '';
		elements += `
		<div class="message-container">
			<div class="message-header">
				<div class="message-user-image">
					<img class="rounded-messages" src="${message.ImageUrl ? message.ImageUrl : noImage}" alt="profile picture" />
				</div>
				<div class="message-user-info">
					<span class="message-username">${message.UserFirstName + ' ' + message.UserLastName + ' ('}<a href="/Users/Details/${message.UserId}">@${message.UserPseudo}</a>${')'}</span>
					<span class="message-date">${date.toLocaleDateString() + ' à ' + date.toLocaleTimeString()}</span>
				</div>
			</div>
			<div class="message-body">
				<span>${message.Content}</span>
			</div>
			<div class="message-footer">
				<div>
					<div class="padding-left-md">
						<div>
							<input type="hidden" value="${message.Id}" />
							<a href="/Messages/Details/${message.Id}"><i class="fa fa-search margin-right-sm details-icon" data-toggle="tooltip" data-placement="bottom" title="Détails"></i></a>
							<i class="inline-block fa fa-heart like ${message.IsLiked ? 'liked' : ''}"></i>
							<p class="inline-block counter like-color">
								${message.Likes.length}
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>`;
	});
	$('div.timeline').append(elements);
}