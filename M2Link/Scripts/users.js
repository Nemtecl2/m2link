﻿$(function () {
    initUsers();
});

function follow(id, el) {
    $.ajax({
        type: 'POST',
        url: '/Users/Follow',
        dataType: 'json',
        data: { subscriptionId: id },
        success: function (data) {
            $(el).addClass('followed');
            toastr.success(data.Message, null, toastrOptions());
            initUsers();
        },
        error: function (ex) {
            console.error(ex);
            toastr.error("Erreur lors de la tentative de désabonnement", null, toastrOptions());
        }
    });
    return false;
}

function unfollow(id, el) {
    $.ajax({
        type: 'POST',
        url: '/Users/Unfollow',
        dataType: 'json',
        data: { subscriptionId: id },
        success: function (data) {
            $(el).removeClass('followed');
            toastr.success(data.Message, null, toastrOptions());
            initUsers();
        },
        error: function (ex) {
            console.error(ex);
            toastr.error("Erreur lors de la tentative d'abonnement", null, toastrOptions());
        }
    });
    return false;
}

function initUsers() {
    $('.followable:not(.followed)')
        .off('click')
        .on('click', function () {
            var id = $(this).siblings('input[type=hidden]').val();
            follow(id, this);
        });

    $('.followable.followed')
        .off('click')
        .on('click', function () {
            var id = $(this).siblings('input[type=hidden]').val();
            unfollow(id, this);
        });
}
