﻿function like(id, el) {
    $.ajax({
        type: 'POST',
        url: '/Messages/Like',
        dataType: 'json',
        data: { messageId: id },
        success: function (data) {
            if (data.Success) {
                $(el).addClass('liked');
                $(el).siblings('.counter').text(data.Count);
                initMessages();
            } else {
                toastr.error("Vous aimez déjà ce tweet", null, toastrOptions());
            }
        },
        error: function (ex) {
            console.error(ex);
        }
    });
    return false;
}

function dislike(id, el) {
    $.ajax({
        type: 'POST',
        url: '/Messages/Dislike',
        dataType: 'json',
        data: { messageId: id },
        success: function (data) {
            if (data.Success) {
                $(el).removeClass('liked');
                $(el).siblings('.counter').text(data.Count);
                initMessages();
            } else {
                toastr.error("Vous n'aimez déjà pas ce tweet", null, toastrOptions());
            }
        },
        error: function (ex) {
            console.error(ex);
        }
    });
    return false;
}

function initMessages() {
    $('.like:not(.liked)')
        .off('click')
        .on('click', function (e) {
            e.stopPropagation();
            var id = $(this).siblings('input[type=hidden]').val();
            like(id, this);
        });

    $('.like.liked')
        .off('click')
        .on('click', function (e) {
            e.stopPropagation();
            var id = $(this).siblings('input[type=hidden]').val();
            dislike(id, this);
        });

    $('.details-icon')
        .off('click')
        .on('click', function () {
            var id = $(this).siblings('input[type=hidden]').val();
            window.location.href = "Messages/Details/" + id;
        });
}
