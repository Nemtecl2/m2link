﻿using System;

namespace M2Link.WebSite.ViewModels
{
    public class LikeModel
    {
        public Guid Id { get; set; }
        public Guid MessagesId { get; set; }
        public Guid UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserPseudo { get; set; }
        public string UserImageUrl { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}