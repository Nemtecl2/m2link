﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace M2Link.WebSite.ViewModels
{
    public class MessagesModel
    {
        public List<MessageModel> Messages { get; set; }

        public MessagesModel()
        {
            Messages = new List<MessageModel>();
        }
    }

    public class MessageModel
    {
        public Guid Id { get; set; }
        [Required]
        [DisplayName("Contenu")]
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid UserId { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public string UserPseudo { get; set; }
        public string ImageUrl { get; set; }
        public bool IsLiked { get; set; }
        public List<LikeModel> Likes { get; set; }
        public MessageModel()
        {
            Likes = new List<LikeModel>();
        }
    }
}