﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace M2Link.WebSite.ViewModels
{
    public class FollowableModel
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        [DisplayName("Pseudo")]
        public string Pseudo { get; set; }
        [Required]
        [DisplayName("Prénom")]
        public string FirstName { get; set; }
        [Required]
        [DisplayName("Nom de famille")]
        public string LastName { get; set; }
        [Required]
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required]
        [DisplayName("Date de création")]
        public DateTime CreatedAt { get; set; }
        [Required]
        public bool? IsFollowed { get; set; }
        public string ImageUrl { get; set; }
    }
}