﻿using M2Link.WebSite.Util;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace M2Link.WebSite.ViewModels
{
    public class RegisterModel
    {
        [Required]
        [DisplayName("Prénom")]
        public string FirstName { get; set; }
        [Required]
        [DisplayName("Nom de famille")]
        public string LastName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$", 
            ErrorMessage = "Le champs email doit contenir un email")]
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required]
        [DisplayName("Pseudonyme")]
        public string Pseudo { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#$^+=!*()@%&]).{8,}$", 
            ErrorMessage = "Le mot de passe doit contenir au moins une majuscule, " +
            "une minuscule, un chiffre et un caractère spécial et de longueur 8 minimum")]
        [DisplayName("Mot de passe")]

        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Verification du mot de passe")]
        [Compare("Password")]
        public string VerifyPassword { get; set; }
        [Image(AllowedFileExtensions = new string[] { ".jpg", ".png",}, MaxWidth = 300, MaxHeight = 300)]
        [DisplayName("Photo de profil")]
        public HttpPostedFileBase File { get; set; }
    }
}