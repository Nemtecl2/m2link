﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace M2Link.WebSite.ViewModels
{
    public class LoginModel
    {
        [Required]
        [DisplayName("Pseudonyme")]
        public string Pseudo { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [DisplayName("Mot de passe")]
        public string Password { get; set; }
    }
}