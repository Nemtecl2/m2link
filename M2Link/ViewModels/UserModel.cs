﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace M2Link.WebSite.ViewModels
{
    public class UserModel
    {
        public Guid Id { get; set; }

        [DisplayName("Prénom")]
        public string FirstName { get; set; }
        [DisplayName("Nom de famille")]
        public string LastName { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }
        [DisplayName("Pseudonyme")]
        public string Pseudo { get; set; }
        [DisplayName("Description")]
        public string Description { get; set; }
        [DisplayName("Photo de profil")]
        public string ImageUrl { get; set; }
        [DisplayName("Date de création")]
        public DateTime CreatedAt { get; set; }
        [DisplayName("Messages")]
        public MessagesModel Messages { get; set; }        
        [DisplayName("Abonnements")]
        public List<FollowableModel> Subscriptions { get; set; }
        public bool? IsFollowed { get; set; }



        public UserModel() : base()
        {

        }
    }
}