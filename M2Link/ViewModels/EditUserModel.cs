﻿using M2Link.Domain;
using M2Link.WebSite.Util;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace M2Link.WebSite.ViewModels
{
    public class EditUserModel
    {
        [Required]
        [DisplayName("Prénom")]
        public string FirstName { get; set; }

        [Required]
        [DisplayName("Nom de famille")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$",
            ErrorMessage = "Le champs email doit contenir un email")]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Pseudonyme")]
        public string Pseudo { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        
        [Image(AllowedFileExtensions = new string[] { ".jpg", ".png", }, MaxWidth = 300, MaxHeight = 300)]
        [DisplayName("Photo de profil")]
        public HttpPostedFileBase File { get; set; }

        [DisplayName("Photo de profil")]
        public string ImageUrl { get; set; }

        public EditUserModel() : base()
        {

        }

        public EditUserModel(User user) : base()
        {
            FirstName = user.FirstName;
            LastName = user.LastName;
            Email = user.Email;
            Pseudo = user.Pseudo;
            Description = user.Description;
            ImageUrl = user.Image == null ? Document.NoImage : "data:image;base64," + Convert.ToBase64String(user.Image.Byte);
        }

    }
}