﻿using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Web;
namespace M2Link.WebSite.Util
{

    public class ImageAttribute : ValidationAttribute
    {
        public string[] AllowedFileExtensions;
        public string[] AllowedContentTypes;
        public int MaxWidth;
        public int MaxHeight;

        public override bool IsValid(object value)
        {

            HttpPostedFileBase file = value as HttpPostedFileBase;

            //this should be handled by [Required]
            if (file == null)
                return true;

            if (AllowedFileExtensions != null)
            {
                if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                {
                    ErrorMessage = "Le fichier doit avoir l'une des extensions suivantes : " + string.Join(", ", AllowedFileExtensions);
                    return false;
                }
            }

            if (AllowedContentTypes != null)
            {
                if (!AllowedContentTypes.Contains(file.ContentType))
                {
                    ErrorMessage = "Le fichier doit avoir l'une des extensions suivantes : " + string.Join(", ", AllowedContentTypes);
                    return false;
                }
            }

            var image = new Bitmap(file.InputStream);

            if (image.Height > MaxHeight || image.Width > MaxWidth)
            {
                ErrorMessage = "Le fichier doit avoir au maximum les dimensions suivantes : " + MaxWidth + "x" + MaxHeight;
                return false;
            }

            return true;
        }
    }
}